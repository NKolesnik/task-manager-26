package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

}
